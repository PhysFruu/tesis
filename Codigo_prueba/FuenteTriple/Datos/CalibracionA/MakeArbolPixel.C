#define MakeArbolPixel_cxx
// The class definition in MakeArbolPixel.h has been generated automatically
// by the ROOT utility TTree::MakeSelector(). This class is derived
// from the ROOT class TSelector. For more information on the TSelector
// framework see $ROOTSYS/README/README.SELECTOR or the ROOT User Manual.

// The following methods are defined in this file:
//    Begin():        called every time a loop on the tree starts,
//                    a convenient place to create your histograms.
//    SlaveBegin():   called after Begin(), when on PROOF called only on the
//                    slave servers.
//    Process():      called for each event, in this function you decide what
//                    to read and fill your histograms.
//    SlaveTerminate: called at the end of the loop on the tree, when on PROOF
//                    called only on the slave servers.
//    Terminate():    called at the end of the loop on the tree,
//                    a convenient place to draw/fit your histograms.
//
// To use this file, try the following session on your Tree T:
//
// Root > T->Process("MakeArbolPixel.C")
// Root > T->Process("MakeArbolPixel.C","some options")
// Root > T->Process("MakeArbolPixel.C+")
//

#include "funcionesImportantes.C"
#include "MakeArbolPixel.h"
#include <TH2.h>
#include <TStyle.h>
int canalPAD=15;
int primerStripP=16;
int primerStripN=48;
Float_t minfiltro=30000;
Float_t maxfiltro=500000;
void MakeArbolPixel::Begin(TTree * /*tree*/)
{
   // The Begin() function is called at the start of the query.
   // When running with PROOF Begin() is only called on the client.
   // The tree argument is deprecated (on PROOF 0 is passed).

   TString option = GetOption();

   tf1 = new TFile("TreePixel.root","recreate");



//    //Definición de mapa
    mapa = new TH2F("Mapa","Mapa del DSSSD",18,0,18,18,0,18);
      mapa->GetXaxis()->SetTitle("Lado p");
      mapa->GetYaxis()->SetTitle("Lado n");
      mapa->GetZaxis()->SetTitle("Cuentas");
      mapa->SetTitleSize(.035,"xyz");

//    for(int i=0; i<Detectores; i++) {
//       // Le damos titulo
//       const char* contador=intToConstChar(i);
//       const char* contador2=intToConstChar(i-16);

//       if(i==0){
//          const char* titulohb1="Histograma del PAD ";
//          const char* indicadorhb1="HistoPAD";
//          const char* titulohbcorr="Bidimencional del Strip PAD vs PAD ";
//          const char* indicadorhbcorr="BidiPAD";
//       //Definir los objetos para los diagramas
//          hb1[i] = new TH1F(indicadorhb1,titulohb1, 5000,0, 3200000);
//          hbcorr[i] = new TH2F(indicadorhbcorr,titulohbcorr,200,0,500000,200,0,500000);

//          hbcorr[i]->GetXaxis()->SetTitle("Canal PAD");
//          hbcorr[i]->GetYaxis()->SetTitle("Canal PAD");
//          hbcorr[i]->GetZaxis()->SetTitle("Cuentas");
//          hbcorr[i]->SetTitleSize(.035,"xyz");

//          hb1[i]->GetXaxis()->SetTitle("Canal PAD");
//          hb1[i]->GetYaxis()->SetTitle("Cuentas");
//          hb1[i]->SetTitleSize(.035,"xy");
//       }
//       else if(i>0&&i<=16)
//       {
//          const char* titulohb1=concatenarChar("Histograma del Strip P",contador);
//          const char* indicadorhb1=concatenarChar("HistoP",contador);
//          const char* titulohbcorr=concatenarChar("Bidimencional del Strip P",contador);
//          const char* indicadorhbcorr=concatenarChar("BidiP",contador);

//       //Definir los objetos para los diagramas
//          hb1[i] = new TH1F(indicadorhb1,titulohb1, 5000,0, 3200000);
//          hbcorr[i] = new TH2F(indicadorhbcorr,titulohbcorr,200,0,500000,200,0,500000);

// //Titulos de los gráficos
//          const char* tituloP=concatenarChar("Canal P",contador);

//          hbcorr[i]->GetXaxis()->SetTitle("Canal PAD");
//          hbcorr[i]->GetYaxis()->SetTitle(tituloP);
//          hbcorr[i]->GetZaxis()->SetTitle("Cuentas");
//          hbcorr[i]->SetTitleSize(.035,"xyz");

//          hb1[i]->GetXaxis()->SetTitle(tituloP);
//          hb1[i]->GetYaxis()->SetTitle("Cuentas");
//          hb1[i]->SetTitleSize(.035,"xy");
//       }
//       else if(i>16){
//          const char* titulohb1=concatenarChar("Histograma del Strip N",contador2);
//          const char* indicadorhb1=concatenarChar("HistoN",contador2);
//          const char* titulohbcorr=concatenarChar("Bidimencional del Strip N",contador2);
//          const char* indicadorhbcorr=concatenarChar("BidiN",contador2);
//       //Definir los objetos para los diagramas
//          hb1[i] = new TH1F(indicadorhb1,titulohb1, 5000,0, 3200000);
//          hbcorr[i] = new TH2F(indicadorhbcorr,titulohbcorr,200,0,500000,200,0,500000);

// //Titulos de los gráficos
//          const char* tituloN=concatenarChar("Canal N",contador2);

//          hbcorr[i]->GetXaxis()->SetTitle("Canal PAD");
//          hbcorr[i]->GetYaxis()->SetTitle(tituloN);
//          hbcorr[i]->GetZaxis()->SetTitle("Cuentas");
//          hbcorr[i]->SetTitleSize(.035,"xyz");

//          hb1[i]->GetXaxis()->SetTitle(tituloN);
//          hb1[i]->GetYaxis()->SetTitle("Cuentas");
//          hb1[i]->SetTitleSize(.035,"xy");
//       }


//    }
   int i=0;
      while(i<Pixeles) {
         for(int p=1; p<Strip;p++){
            for(int n=1;n<Strip;n++){
               const char* contadorn;
               const char* contadorp;
               if (p<10){
                  contadorp=concatenarChar("0",intToConstChar(p));
               }
               else
               {
                  contadorp=intToConstChar(p);
               }

               if (n<10){
                  contadorn=concatenarChar("0",intToConstChar(n));
               }
               else
               {
                  contadorn=intToConstChar(n);
               }

               const char* IDTree;
               const char* tituloTree;
               IDTree=concatenarChar(contadorp,contadorn);
               tituloTree=concatenarChar("Arbol ",IDTree);
               //prueba const char* contadorn=intToConstChar(i);
               TreePixel[p][n] = new TNtuple(IDTree, tituloTree, "E:DE");
               const char* titulohbpixelcorr;
               const char* indicadorhbpixelcorr;
               const char* indicadorP;
               const char* indicadorN;
               indicadorP=concatenarChar("P",contadorp);
               indicadorN=concatenarChar("N",contadorn);
               indicadorP=concatenarChar(indicadorP,indicadorN);
               titulohbpixelcorr=concatenarChar("Bidimensional del pixel ",indicadorP);
               indicadorhbpixelcorr=concatenarChar("B",IDTree);
               pixel[i] = new TH2F(indicadorhbpixelcorr,titulohbpixelcorr,200,0,5,200,0,5);
               //pixel[i] = new TH2F(i,titulohbpixelcorr,1000,0,400000,1000,0,400000);

                pixel[i]->GetXaxis()->SetTitle("E [MeV]");
                pixel[i]->GetYaxis()->SetTitle("Delta E [MeV]");
                pixel[i]->GetZaxis()->SetTitle("Cuentas");
                pixel[i]->SetTitleSize(.035,"xyz");

                 i=i+1;
            }
         }
      }
}

void MakeArbolPixel::SlaveBegin(TTree * /*tree*/)
{
   // The SlaveBegin() function is called after the Begin() function.
   // When running with PROOF SlaveBegin() is called on each slave server.
   // The tree argument is deprecated (on PROOF 0 is passed).

   TString option = GetOption();

}

Bool_t MakeArbolPixel::Process(Long64_t entry)
{
   // The Process() function is called for each entry in the tree (or possibly
   // keyed object in the case of PROOF) to be processed. The entry argument
   // specifies which entry in the currently loaded tree is to be processed.
   // It can be passed to either MakeArbolPixel::GetEntry() or TBranch::GetEntry()
   // to read either all or the required parts of the data. When processing
   // keyed objects with PROOF, the object is already loaded and is available
   // via the fObject pointer.
   //
   // This function should contain the "body" of the analysis. It can contain
   // simple or elaborate selection criteria, run algorithms on the data
   // of the event and typically fill histograms.
   //
   // The processing can be stopped by calling Abort().
   //
   // Use fStatus to set the return value of TTree::Process().
   //
   // The return value is currently not used.
   GetEntry(entry);

   if(entry%10000==0)cout<<"Event:"<<entry<<endl;
   //Se hace el histograma de 1 y 2 dimensiones para cada strip/PAD
    bool ok[33]={ false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };
    Float_t x=0;
    Float_t yp[17];
    Float_t yn[17];
   // Float_t y=0;
    int w=0;
    int n=0;
    double_t calibracion[17][2]={
      {0.0000193085609756098,-1.44234950487805},
      {0,0},
      {0.0000189,-0.567},
      {0.0000181,-0.541},
      {0.0000175,-0.443},
      {0.000018,-0.463},
      {0.0000187,-0.612},
      {0.0000155,-0.214},
      {0.0000185,-0.679},
      {0.0000196,-0.871},
      {0.0000176,-0.41},
      {0.0000183,-0.635},
      {0.0000162,-0.191},
      {0.0000159,-0.176},
      {0.0000194,-0.863},
      {0.0000204,-1.14},
      {0.0000212,-1.35}
    };

   // //Se mete a los registros
    for(int i=0; i<fmulti; i++) {


   //    //Si estoy en el registro del PAD
       if(fchan[i]==canalPAD) {
   //       // se hace el unidimensional
   //       hb1[0]->Fill(fbxch[i]);
   //       // se hace el bidimensional
          x = calibracion[0][0]*fbxch[i]+calibracion[0][1];
   //       hbcorr[0]->Fill(x,x);
   //       //y se genera el parámetro de correlación al 100%
          ok[0] = true;
       }
   //    //se busca en los demás canales
       for (int j=1; j<17; j++){
      //  int j=2;
   //          //codigo que engloba sólo al lado P
            // int j=1; //cerrando el for
             if(fchan[i]==primerStripP-1+j) {
   //             //Se hace el unidimensional
                yp[j] = fbxch[i];
                if(yp[j]>minfiltro&&yp[j]<maxfiltro){
   //                //Estamos en el canal p
                   ok[j] = true;
                     yp[j] = calibracion[j][0]*fbxch[i]+calibracion[j][1];
   //                hb1[j]->Fill(yp[j]);
//    //                //Si estamos en el canal 0 y p [j]
//                    if(ok[0]){
   //                   //Se hace el bidimensional
   //                   hbcorr[j]->Fill(x,yp[j]);
                //      ntupla[1]->Fill(x,yp[j]);
                   }
                }
             //Salimos del lado P


   //       //Codigo que engloba solo al lado N
          if(fchan[i]==primerStripN-1+j) {
   //             //se hace el unidimensional
               if(j<9){
                  n=9-j;

               }
               else if (j>8)
               {
                  n=17-j+8;
               //   n=j-1;
               //   if(j==9)
               //      n=16;
               }


               yn[n] = fbxch[i];
               if(yn[n]>minfiltro&&yn[n]<maxfiltro){
   //                hb1[n+16]->Fill(yn[n]);
                  ok[n+16] = true;
   //                //si estamos en el canal 0 y n [j]
   //                if(ok[0])
   //                   //Se hace el bidimensional
   //                   hbcorr[n+16]->Fill(x,yn[n]);
               }
         }
   // //Salimos de ambos canales
       }
      for (int p = 1; p < 17; p++)
      {
         for (int q = 1; q < 17; q++)
         {
            /* code */
            if(ok[p]&&ok[q+16]&&ok[0]){
               w=16*(p-1)+q-1;
               mapa->Fill(p,q);
               pixel[w]->Fill(x,yp[p]);
               TreePixel[p][q]->Fill(x,yp[p]);
            }
         }
      }

   // //Salimos de todos los registros
   }
   return kTRUE;
}

void MakeArbolPixel::SlaveTerminate()
{
   // The SlaveTerminate() function is called after all entries or objects
   // have been processed. When running with PROOF SlaveTerminate() is called
   // on each slave server.

}

void MakeArbolPixel::Terminate()
{
   // The Terminate() function is the last function to be called during
   // a query. It always runs on the client, it can be used to present
   // the results graphically or save the results to file.

   tf1->Write();
   tf1->Close();
}
