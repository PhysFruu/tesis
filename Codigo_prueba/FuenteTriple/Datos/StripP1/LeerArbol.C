//cd Documents/FFMG/DatosMultiplataforma/FuenteTriple/Datos/StripP1
//root
//#include "LeerArbol.C"
//main()
#include <stdio.h>
#include <TFile.h>
#include <TH1.h>
#include <TF1.h>
#include <THStack.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>
#include "funcionesImportantes.C"
#include <iostream>
#include <math.h>
#define Kvectores 6
#define dimension 2
#define ciclos 5
float x[dimension];
int contador;
float mu[Kvectores][dimension];
float muHistorico[ciclos][Kvectores][dimension];
float pendiente[Kvectores];
TFile *histogramaStrip;
TH2F *ComportamientoMu[Kvectores];
TH2F *HistogramaSegregado[Kvectores];
TH2F *HistogramaTotal;
TF1 *fit;
THStack *hs;

//TFile *myFile = TFile::Open("TreeStrip.root");

//El proposito de esta funcion es extraer el número de eventos del arbol
void Conteo(){
// Create a TTreeReader for the tree, for instance by passing the
// TTree's name and the TDirectory / TFile it is in.
   TFile *myFile = TFile::Open("TreeStrip.root");
   TTreeReader myReader("TreeStrip", myFile);
   // The branch "px" contains floats; access them as myPx.
   TTreeReaderValue<Float_t> E(myReader, "E");
   // The branch "py" contains floats, too; access those as myPy.
   TTreeReaderValue<Float_t> DE(myReader, "DE");
   // Loop over all entries of the TTree or TChain.
   contador=0;
   while (myReader.Next()) {
      // Just access the data as if myPx and myPy were iterators (note the '*'
      // in front of them):
      contador=contador+1;
      //x[0]=*E;
      //x[1]=*DE;
      //cout<<"El vector "<< x[0] <<","<<x[1] <<endl;
   };
   myFile->Close();
};

//devuelve el vector de entrada guardado en x
//debe ser el valor menor que contador
void entrada(	Long64_t 	entry	){
// Create a TTreeReader for the tree, for instance by passing the
// TTree's name and the TDirectory / TFile it is in.
   TFile *myFile = TFile::Open("TreeStrip.root");
   TTreeReader myReader("TreeStrip", myFile);
   // The branch "px" contains floats; access them as myPx.
   TTreeReaderValue<Float_t> E(myReader, "E");
   // The branch "py" contains floats, too; access those as myPy.
   TTreeReaderValue<Float_t> DE(myReader, "DE");
   // Loop over all entries of the TTree or TChain.
   myReader.SetEntriesRange(entry,entry+1);
   while (myReader.Next()) {
      // Just access the data as if myPx and myPy were iterators (note the '*'
      // in front of them):
       x[0]=*E;
       x[1]=*DE;
       //cout<<"El vector "<< x[0] <<","<<x[1] <<endl;
   };
   delete myFile;
};

//Se actualiza el indice bajo una mu
//Se modifica *
int indiceC(Long64_t 	entry 	){
   int indice=0;
   entrada(entry);
   float_t a=pow(pendiente[0]*(x[0]-mu[0][0])-(x[1]-mu[0][1]) ,2)+(pow(pendiente[0],2)+1); //*
//      float_t a=pow(x[0]-mu[0][0],2)+pow(x[1]-mu[0][1],2);
   //cout<<"El valor inicial es"<<a<<endl;
   for (int k=1; k<Kvectores;k++){
        float_t b=pow(pendiente[k]*(x[0]-mu[k][0])-(x[1]-mu[k][1]) ,2)+(pow(pendiente[k],2)+1); //*
//      float_t b=pow(x[0]-mu[k][0],2)+pow(x[1]-mu[k][1],2);
      //cout<<"El valor comparado es"<<b<<endl;
      if(b<a){
         indice=indice+1;
         a=b;
         //cout<<"El valor a comparar es"<<a<<endl;
      }
   } 
   return indice;
};

float deltaK(int c, int i){
   float result;
   if(c==i){
      result=1;
   }
   else{
      result=0;
   }
   return result;
}


// void inicio(){
//    TH2F *Histogram= new TH2F("Total","DE-E+DE del Strip",200,0,500000,200,0,500000);
//    cout<<"Comienza el llenado del histograma: segregación"<<endl;
//    for (int i=0; i<contador;i++){
//       entrada(i);
//       Histogram->Fill(x[0],x[1]);
//    }
//    Histogram->Fit("pol1");
//    TF1 *somefit = Histogram->GetFunction("pol1");
//    for (int k=0; k<Kvectores;k++){
//       pendiente[k]=somefit->GetParameter(1);
//    }
//    delete Histogram;
// }





//funcion principal
int main(){
   //se genera el array que se emplea como promedio inicial
   for (int k=0; k<Kvectores;k++){
      if (k>0){
      muHistorico[0][k][1]=mu[k][1]=mu[k-1][1]+25000;
      }  
      else{
      muHistorico[0][k][1]=mu[k][1]=100000;  
      }
   };
   for (int k=0; k<Kvectores;k++){
      if (k>0){
      muHistorico[0][k][0]=mu[k][0]=mu[k-1][0]+25000;
      }
      else{
      muHistorico[0][k][0]=mu[k][0]=100000;
      }
   };

   //mu[numero de mu][0=x,1=y]

   //se lee el arbol para definir los eventos en un const int
   cout<<"Empieza el primer conteo"<<endl;
   Conteo();
   int const Neventos=contador;
   int c[Neventos];
// Se carga el fit para sacar la primera pendiente
   //inicio();
   for (int k=0; k<Kvectores;k++){
      pendiente[k]=-1;
   }
//  

///////////// Ciclo para aprendizaje
      for (int ciclo=1; ciclo<ciclos;ciclo++){
         cout<<"Comienza el ciclo "<<ciclo+1<<endl;
         //actualización del indice.
         cout<<"Empieza a formarse el indice"<<endl;
         for (int i=0; i<contador;i++){
            int indice=indiceC(i);
            c[i]=indice;
         }
         cout<<"Termina el indice"<<endl;

         //suma de cada conjunto
         for (int k=0; k<Kvectores;k++){
            cout<<"Nuevo k "<<k<<endl;
            float sumaEvento[dimension];
            float suma=0;
            for (int d=0; d<dimension; d++){
               sumaEvento[d]=0;
            }
            for (int i=0; i<contador;i++){
               entrada(i);
               for (int d=0; d<dimension; d++){
                  sumaEvento[d]=sumaEvento[d]+deltaK(c[i],k)*x[d];
               }
            suma=suma+deltaK(c[i],k);
            }
            // ////////////////////////////////
            cout<<"Se crea histograma para nueva pendiente "<<k <<endl;
            TH2F *Histograma= new TH2F("titulo","Titulo",200,0,500000,200,0,500000); 
            cout<<"Comienza el llenado del histograma: segregación "<<k <<endl;
            for (int i=0; i<contador;i++){
               if (deltaK(c[i],k)==1){ 
                  Histograma->Fill(x[0],x[1]);
               }
            }
            Histograma->Fit("pol1");
            TF1 *otherfit = Histograma->GetFunction("pol1");
            float newpar=otherfit->GetParameter(1);
            if (abs(pendiente[k]-newpar)<.01){
               pendiente[k]=newpar; //*
             }
            ////////////////
            cout<<"Son "<<suma<<" elementos para el k"<<k<<endl;
            for (int d=0; d<dimension; d++){
               //se genera el promedio
               muHistorico[ciclo][k][d]=mu[k][d]=sumaEvento[d]/suma;
            }
            cout<<"Se finaliza el k "<<k<<endl;
         }
      }
// ///////////////

//Creación del histograma que muestra el comportamiento del centroide
   histogramaStrip = new TFile("histogramaStripE.root","recreate");
   
//

//       const char* contador=intToConstChar(i);
//          const char* titulohb1="Histograma del PAD ";
//          const char* indicadorhb1="HistoPAD";
//          const char* titulohbcorr="Bidimencional del Strip PAD vs PAD ";
//          const char* indicadorhbcorr="BidiPAD";
//  const char* titulohb1="Histograma del PAD ";
cout<<"+ Histogramas "<<endl;
// for (int k=0; k<Kvectores;k++){
//    const char *kc=intToConstChar(k);
//    cout<<"Creacion del histograma Mu "<<k<<endl;
//    ComportamientoMu[k]= new TH2F(concatenarChar("ComportamientoMu",kc),concatenarChar("Centro de masas del automata ",kc),200,0,500000,200,0,500000);
// }

ComportamientoMu[0]= new TH2F("Mu1","Centro de masas del automata ",200,0,500000,200,0,500000);
ComportamientoMu[1]= new TH2F("Mu2","Centro de masas del automata ",200,0,500000,200,0,500000);
ComportamientoMu[2]= new TH2F("Mu3","Centro de masas del automata ",200,0,500000,200,0,500000);
ComportamientoMu[3]= new TH2F("Mu4","Centro de masas del automata ",200,0,500000,200,0,500000);
ComportamientoMu[4]= new TH2F("Mu5","Centro de masas del automata ",200,0,500000,200,0,500000);
ComportamientoMu[5]= new TH2F("Mu6","Centro de masas del automata ",200,0,500000,200,0,500000);

   // THStack hs("hs","test stacked histograms");
   // cout<<"Comienza el llenado del histograma: comportamiento del centroide"<<endl;
   // for (int c=0; c<ciclos;c++){
   //    for (int k=0; k<Kvectores;k++){
   //       ComportamientoMu[k]->Fill(muHistorico[c][k][0],muHistorico[c][k][1]);
   //    };
   // };
   // hs.Add(ComportamientoMu[0]);
   // hs.Add(ComportamientoMu[2]);
   // hs.Add(ComportamientoMu[1]);
// for (int k=0; k<Kvectores;k++){
//    cout<<"Creacion del histograma K "<<k<<endl;
//    HistogramaSegregado[0]= new TH2F(concatenarChar("K",intToConstChar(k)),concatenarChar("DE-E+DE despúes de K-mean, K ",intToConstChar(k)),200,0,500000,200,0,500000);
// }
//
HistogramaSegregado[0]= new TH2F("K0","DE(U.A.)-E(keV) despúes de K-mean, K ",260,0,500000,260,0,500000);
HistogramaSegregado[1]= new TH2F("K1","DE(U.A.)-E(keV) despúes de K-mean, K ",260,0,500000,260,0,500000);
HistogramaSegregado[2]= new TH2F("K2","DE(U.A.)-E(keV) despúes de K-mean, K ",260,0,500000,260,0,500000);
HistogramaSegregado[3]= new TH2F("K3","DE(U.A.)-E(keV) despúes de K-mean, K ",260,0,500000,260,0,500000);
HistogramaSegregado[4]= new TH2F("K4","DE(U.A.)-E(keV) despúes de K-mean, K ",260,0,500000,260,0,500000);
HistogramaSegregado[5]= new TH2F("K5","DE(U.A.)-E(keV) despúes de K-mean, K ",260,0,500000,260,0,500000);


   HistogramaTotal= new TH2F("Total","DE(U.A.)-E(keV) del Strip",260,0,500000,260,0,500000);

   cout<<"Comienza el llenado del histograma: segregación"<<endl;
   for (int i=0; i<contador;i++){
      entrada(i);
      HistogramaTotal->Fill(x[0],x[1]);
//
      for (int k=0; k<Kvectores;k++){
         if(k==c[i]){
            HistogramaSegregado[k]->Fill(x[0],x[1]);
         }
      }
//
   }
   cout<<"Se finaliza"<<endl;
   histogramaStrip->Write();
   histogramaStrip->Close();


   return -1;
}
