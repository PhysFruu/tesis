//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Jan 31 18:26:21 2019 by ROOT version 5.34/22
// from TTree h1/Root Tree
// found on file: febex3_tree.root
//////////////////////////////////////////////////////////

#ifndef StripP1_h
#define StripP1_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include "TH1F.h"
#include "TH2F.h"
#include <iostream>
#include "TNtuple.h"

#define Detectores 33
#define Pixeles 256
TH1F *hb1[Detectores];
TH2F *hbcorr[Detectores];
TH2F *pixel[Pixeles];
TH2F *mapa;
TFile *tf1;
TFile *tupla;
TNtuple *ntupla[Detectores];

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class StripP1 : public TSelector {
public :

   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   // Declaration of leaf types
 //TMyEvent        *febex3;
   Int_t           fmulti;
   Int_t           fchan[6];   //[fmulti]
   Int_t           fbxch[6];   //[fmulti]

   // List of branches
   TBranch        *b_febex3_fmulti;   //!
   TBranch        *b_fchan;   //!
   TBranch        *b_fbxch;   //!

   StripP1(TTree * /*tree*/ =0) : fChain(0) { }
   virtual ~StripP1() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(StripP1,0);
};

#endif

#ifdef StripP1_cxx
void StripP1::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("fmulti", &fmulti, &b_febex3_fmulti);
   fChain->SetBranchAddress("fchan", fchan, &b_fchan);
   fChain->SetBranchAddress("fbxch", fbxch, &b_fbxch);
}

Bool_t StripP1::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

#endif // #ifdef StripP1_cxx
