int longitudInt(int num){ 
    char testing[100];
    sprintf(testing,"%d",num);
    int length = strlen(testing);
    return length;
}

const char* intToConstChar(int n){
    int i = n;
    char * iach= new char[longitudInt(n)+1];
    sprintf(iach, "%d", i);
    const char* ic=iach;
    return ic;
}

int longitudDouble(double num){ 
    char testing[250];
    sprintf(testing,"%.6f",num);
    int length = strlen(testing);
    return length;
}

const char* doubleToConstChar(double n){
    double i = (double) n;
    char * iach= new char[longitudDouble(n)+1];
    sprintf(iach, "%.6f", i);
    const char* ic=iach;
    return ic;
}

const char* concatenarChar(const char* ctr1,const char* ctr2){
    const char* ruta1 = ctr1;
    const char* ruta2 = ctr2;
    char * RutaFinal = new char[strlen(ruta1) + strlen(ruta2) + 1];
    strcpy(RutaFinal, ruta1);
    strcat(RutaFinal, ruta2);
    const char* result=RutaFinal;
    return result;
}


float constCharToFloat(const char* ch){
    return atof(ch);
    }