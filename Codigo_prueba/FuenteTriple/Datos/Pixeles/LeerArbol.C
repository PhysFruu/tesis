//cd Documents/FFMG/DatosMultiplataforma/FuenteTriple/Datos/StripP1
//root
//#include "LeerArbol.C"
//main()
#include <stdio.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TF1.h>
#include <THStack.h>
#include <TTreeReader.h>
#include <TGraph.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>
#include "funcionesImportantes.C"
#include <iostream>
#include <math.h>
#define Kvectores 6
#define dimension 2
#define ciclos 10
#define alfas 3
#define strips 17
float x[dimension];
int contador;
float mu[Kvectores][dimension];
float muHistorico[ciclos][Kvectores][dimension];
float pendiente[Kvectores];
int nEventosK[Kvectores];
int indiceKGrande[Kvectores];
const char* TítuloTreeRead="TreePixel.root";
const char* TítuloTreeResult="histogramasPixel.root";
const char* Ejemplo="0708";
const char* TituloResult;

///***************************************************************************
//El proposito de esta funcion es extraer el número de eventos del arbol
void Conteo(const char* pixelTree){
// Create a TTreeReader for the tree, for instance by passing the
// TTree's name and the TDirectory / TFile it is in.
   TFile *myFile = TFile::Open(TítuloTreeRead);
   TTreeReader myReader(pixelTree, myFile);
   // The branch "px" contains floats; access them as myPx.
   TTreeReaderValue<Float_t> E(myReader, "E");
   // The branch "py" contains floats, too; access those as myPy.
   TTreeReaderValue<Float_t> DE(myReader, "DE");
   // Loop over all entries of the TTree or TChain.
   contador=0;
   while (myReader.Next()) {
      // Just access the data as if myPx and myPy were iterators (note the '*'
      // in front of them):
      contador=contador+1;
      //x[0]=*E;
      //x[1]=*DE;
      //cout<<"El vector "<< x[0] <<","<<x[1] <<endl;
   };
   myFile->Close();
   delete myFile;
};

//*********************************************************
//devuelve el vector de entrada guardado en x
//debe ser el valor menor que contador
void entrada(	Long64_t 	entry	,const char* pixelTree){
// Create a TTreeReader for the tree, for instance by passing the
// TTree's name and the TDirectory / TFile it is in.
   TFile *myFile = TFile::Open(TítuloTreeRead);
   TTreeReader myReader(pixelTree, myFile);
   // The branch "px" contains floats; access them as myPx.
   TTreeReaderValue<Float_t> E(myReader, "E");
   // The branch "py" contains floats, too; access those as myPy.
   TTreeReaderValue<Float_t> DE(myReader, "DE");
   // Loop over all entries of the TTree or TChain.
   myReader.SetEntriesRange(entry,entry+1);
   while (myReader.Next()) {
      // Just access the data as if myPx and myPy were iterators (note the '*'
      // in front of them):
       x[0]=*E;
       x[1]=*DE;
       //cout<<"El vector "<< x[0] <<","<<x[1] <<endl;
   };
   myFile->Close();
   delete myFile;
};
////***********************************************************


//Se actualiza el indice bajo una mu
//Se modifica *
int indiceC(Long64_t 	entry 	,const char* pixelTree){
   int indice=0;
   entrada(entry,pixelTree);
   float_t a=pow(pendiente[0]*(x[0]-mu[0][0])-(x[1]-mu[0][1]) ,2)+(pow(pendiente[0],2)+1); //*
//      float_t a=pow(x[0]-mu[0][0],2)+pow(x[1]-mu[0][1],2);
   //cout<<"El valor inicial es"<<a<<endl;
   for (int k=1; k<Kvectores;k++){
        float_t b=pow(pendiente[k]*(x[0]-mu[k][0])-(x[1]-mu[k][1]) ,2)+(pow(pendiente[k],2)+1); //*
//      float_t b=pow(x[0]-mu[k][0],2)+pow(x[1]-mu[k][1],2);
      //cout<<"El valor comparado es"<<b<<endl;
      if(b<a){
         indice=indice+1;
         a=b;
         //cout<<"El valor a comparar es"<<a<<endl;
      }
   } 
   return indice;
};

//Delta que es 1 si el valor del indice corresponde al otro
float deltaK(int c, int i){
   float result;
   if(c==i){
      result=1;
   }
   else{
      result=0;
   }
   return result;
}

//Se intenta ocupar el fit lineal de toda la estadistica. Se generan errores graves en las formas de las lineas
// void inicio(){
//    TH2F *Histogram= new TH2F("Total","E-DE del Strip",200,0,500000,200,0,500000);
//    cout<<"Comienza el llenado del histograma: segregación"<<endl;
//    for (int i=0; i<contador;i++){
//       entrada(i);
//       Histogram->Fill(x[0],x[1]);
//    }
//    Histogram->Fit("pol1");
//    TF1 *somefit = Histogram->GetFunction("pol1");
//    for (int k=0; k<Kvectores;k++){
//       pendiente[k]=somefit->GetParameter(1);
//    }
//    delete Histogram;
// }





//funcion principal
void logica(const char* pixelTree){

   cout<<"Comienza el analisis del pixel: "<<pixelTree<<endl;

   TFile *FilePixel;
   TGraph *ComportamientoMu[Kvectores];
   TH2F *HistogramaSegregado[Kvectores];
   TH2F *HistogramaAlfa[alfas];
   TH2F *HistogramaTotal;
   TF1 *fit;
   TH1D *hx[alfas];
   TH1D *hy[alfas];
   TF1 *FitY[alfas];
   TF1 *FitX[alfas];


   //se genera el array que se emplea como promedio inicial mu
   for (int k=0; k<Kvectores;k++){
      if (k>0){
      muHistorico[0][k][1]=mu[k][1]=mu[k-1][1]+0;
      }  
      else{
      muHistorico[0][k][1]=mu[k][1]=100000;  
      }
   };
   for (int k=0; k<Kvectores;k++){
      if (k>0){
      muHistorico[0][k][0]=mu[k][0]=mu[k-1][0]+25000;
      }
      else{
      muHistorico[0][k][0]=mu[k][0]=250000;
      }
   };
   //Donde mu[numero de mu][0=x,1=y]
   //Fin

   //se lee el arbol para definir los eventos en un const int
   cout<<"Empieza el primer conteo"<<endl;
   Conteo(pixelTree);
   int const Neventos=contador;
   int c[Neventos];
// Se carga el fit para sacar la primera pendiente
   //inicio();
   for (int k=0; k<Kvectores;k++){
      pendiente[k]=-1;
   }
//  

///////////// Ciclo para aprendizaje
      for (int ciclo=1; ciclo<ciclos;ciclo++){
         cout<<"Comienza el ciclo "<<ciclo+1<<endl;
         //actualización del indice.
         cout<<"Empieza a formarse el indice"<<endl;
         for (int i=0; i<contador;i++){
            int indice=indiceC(i,pixelTree);
            c[i]=indice;
         }
         cout<<"Termina el indice"<<endl;

         //suma de cada conjunto
         for (int k=0; k<Kvectores;k++){
            cout<<"Nuevo k "<<k<<endl;
            float sumaEvento[dimension];
            nEventosK[k]=0;
            for (int d=0; d<dimension; d++){
               sumaEvento[d]=0;
            }
            for (int i=0; i<contador;i++){
               entrada(i,pixelTree);
               for (int d=0; d<dimension; d++){
                  sumaEvento[d]=sumaEvento[d]+deltaK(c[i],k)*x[d];
               }
            nEventosK[k]=nEventosK[k]+deltaK(c[i],k);
            }
         //Actualizacion de pendiente. Mala idea.
            // ////////////////////////////////
            // cout<<"Se crea histograma para nueva pendiente "<<k <<endl;
            // TH2F *Histograma= new TH2F("titulo","Titulo",200,0,500000,200,0,500000); 
            // cout<<"Comienza el llenado del histograma: segregación "<<k <<endl;
            // for (int i=0; i<contador;i++){
            //     entrada(i);
            //    if (deltaK(c[i],k)==1){ 
            //       Histograma->Fill(x[0],x[1]);
            //    }
            // }
            // Histograma->Fit("pol1");
            // TF1 *otherfit = Histograma->GetFunction("pol1");
            // float newpar=otherfit->GetParameter(1);
            // if (abs(pendiente[k]-newpar)<.1){
            //    pendiente[k]=newpar; //*
            //  }
            // cout<<newpar<<endl;
            // delete Histograma;
            ////////////////
            cout<<"Son "<<nEventosK[k]<<" elementos para el k"<<k<<endl;
            for (int d=0; d<dimension; d++){
               //se genera el promedio
               muHistorico[ciclo][k][d]=mu[k][d]=sumaEvento[d]/nEventosK[k];
            }
            cout<<"Se finaliza el k "<<k<<endl;
         }
      }
// ///////////////

//Creación del histograma que muestra el comportamiento del centroide
   //FilePixel = new TFile(TítuloTreeResult,"update");
   TituloResult=concatenarChar(pixelTree,".root");
   FilePixel = new TFile(TituloResult,"recreate");

   
//

//       const char* contador=intToConstChar(i);
//          const char* titulohb1="Histograma del PAD ";
//          const char* indicadorhb1="HistoPAD";
//          const char* titulohbcorr="Bidimencional del Strip PAD vs PAD ";
//          const char* indicadorhbcorr="BidiPAD";
//  const char* titulohb1="Histograma del PAD ";
   cout<<"+ Histogramas "<<endl;

// se cargan los histogramas en el .root
   for(int k=0; k<Kvectores; k++) {
      const char* countK=intToConstChar(k);
      const char* indicadorK1=concatenarChar(pixelTree,concatenarChar("Mu",countK));
      const char* indicadorK2=concatenarChar(pixelTree,concatenarChar("K",countK));
      
      ComportamientoMu[k]= new TGraph(ciclos);
      ComportamientoMu[k]->SetName(indicadorK1);
      HistogramaSegregado[k]= new TH2F(indicadorK2,indicadorK2,260,0,500000,260,0,500000);
      
   }
      HistogramaTotal= new TH2F(pixelTree,concatenarChar("E-DE del pixel ",pixelTree),260,0,500000,260,0,500000);
      HistogramaTotal->GetXaxis()->SetTitle("E");
      HistogramaTotal->GetYaxis()->SetTitle("Delta E");
      HistogramaTotal->GetZaxis()->SetTitle("Número de cuentas");
      

   for(int a=0; a<alfas; a++) {
      const char* countK=intToConstChar(a);
      const char* indicadorA=concatenarChar(pixelTree,countK);

      HistogramaAlfa[a]=new TH2F(indicadorA,concatenarChar("Alfa ",concatenarChar(countK, concatenarChar(" del pixel ",pixelTree))),200,0,500000,200,0,500000);
      HistogramaAlfa[a]->GetXaxis()->SetTitle("E");
      HistogramaAlfa[a]->GetYaxis()->SetTitle("Delta E");
      HistogramaAlfa[a]->GetZaxis()->SetTitle("Número de cuentas");

      const char* indicadorX=concatenarChar(indicadorA,"x");
      const char* indicadorY=concatenarChar(indicadorA,"y");

      hx[a] = new TH1D(indicadorX,indicadorX, 250,0, 500000);
      hy[a] = new TH1D(indicadorY,indicadorY, 250,0, 500000);

      hx[a]->GetXaxis()->SetTitle("E");
      hy[a]->GetXaxis()->SetTitle("Delta E");
      gStyle->SetOptFit();

      const char* indicadorFX=concatenarChar(indicadorA,"0");
      const char* indicadorFY=concatenarChar(indicadorA,"1");
      //if (a==0){
      //   FitY[a] = new TF1(indicadorFX,"gaus",50000,150000);
      //   FitX[a] = new TF1(indicadorFY,"gaus",240000,280000);
      //}
      //else
      //{ 
         FitY[a] = new TF1(indicadorFX,"gaus",50000,300000);
         FitX[a] = new TF1(indicadorFY,"gaus",50000,300000);
      //}
      
   }

   // THStack hs("hs","test stacked histograms");
   cout<<"Comienza el llenado del gráfico: comportamiento del centroide"<<endl;
   for (int k=0; k<Kvectores;k++){
      for (int c=0; c<ciclos;c++){
         ComportamientoMu[k]->SetPoint(c,muHistorico[c][k][0],muHistorico[c][k][1]);
      };
      ComportamientoMu[k]->Write();
   };


//Se hace la comparación de los 3K's más grandes
   for (int k=0; k<Kvectores;k++){
      indiceKGrande[k]=0;
         for (int k2=0; k2<Kvectores;k2++){
            if(nEventosK[k]>nEventosK[k2]){
               indiceKGrande[k]=indiceKGrande[k]+1;
            }
         }
   }
//Se llenan los histogramas
   cout<<"Comienza el llenado del histograma total, k clusters y las 3 alfas"<<endl;
   for (int i=0; i<contador;i++){
      entrada(i,pixelTree);
      HistogramaTotal->Fill(x[0],x[1]);
// 
      for (int k=0; k<Kvectores;k++){
         if(k==c[i]){
            HistogramaSegregado[k]->Fill(x[0],x[1]);
                for (int c=0; c<alfas;c++){
                  if(c+Kvectores-alfas==indiceKGrande[k]){
                     HistogramaAlfa[c]->Fill(x[0],x[1]);
                     hx[c]->Fill(x[0]);
                     hy[c]->Fill(x[1]);
                  }
                }
         }
      }
//
   }

//Se genera el Fit
   cout<<"Se crean los fits de la proyeccion del pixel"<<pixelTree<<endl;
   for (int a=0; a<alfas;a++){
      hx[a]->Fit(FitY[a],"R");
      hy[a]->Fit(FitX[a],"R");
   }


   cout<<"Se finaliza el pixel"<<pixelTree<<endl;

   //Se cierra el arbol
   FilePixel->Write();
   FilePixel->Close();

   //se borra de memoria
   delete FilePixel;
}

int main(){
   const char* contadorn;
   const char* contadorp;
   const char* titulo;
   for(int p=2; p<strips; p++) {
      for(int n=2; n<strips-1; n++) {
         if (p<10){
            contadorp=concatenarChar("0",intToConstChar(p));
         }
         else
         {
            contadorp=intToConstChar(p);
         }

         if (n<10){
            contadorn=concatenarChar("0",intToConstChar(n));
         }
         else
         {
            contadorn=intToConstChar(n);
         }
         titulo=concatenarChar(contadorp,contadorn);
         logica(titulo);
      }
   }


   return -1;
}

