//CORRER EL SCRIPT PARA GENERAR TODOS LOS HISTOGRAMAS
//febex3_tree.root es la base de todo.
//Primer proceso
root
TFile *file=new TFile("febex3_tree.root")
TTree *arbol=(TTree *)file->Get("h1")
h1->Process("StripsEnergy.C+")
.q

//Segundo proceso
root
TFile *file=new TFile("febex3_tree.root")
TTree *arbol=(TTree *)file->Get("h1")
h1->Process("MakeArbolPixel.C+")
new TBrowser

#include "LeerArbol.C"
main()
.q

//Tercer proceso
root
#include "readerEachPixel.C"
extraerAllFitaCSV()
leerTXT()

//Cuarto proceso cuando ya se analizan los .csv
root
TFile *file=new TFile("febex3_tree.root")
TTree *arbol=(TTree *)file->Get("h1")
h1->Process("Calibracion.C+")
new TBrowser

